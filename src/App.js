import React from 'react';
import Routes from "./ConvertJsx/Routes";
import HeaderComponent from "./ConvertJsx/HeaderComponent";
import './App.css';

import styles from './teststyle.module.css'; 

import { BrowserRouter as Router } from "react-router-dom";

function App() {
  return (

    <>
      <Router>
        <HeaderComponent/>
        <Routes/>
      </Router>

    </>
  );
}
  
export default App;



/*import React from 'react';
import logo from './logo.png';
import './App.css';
import DaftarFilm from './ConvertJsx/DaftarFilm';

function App() {
  return (
    <div className="DaftarFilm">
      <header>
        <header_left />
        <header_right/>
      </header>
    </div>
  );
}

export default App;*/
