import React from "react";
import {Link} from "react-router-dom";
import styles from '../teststyle.module.css';

const HeaderComponent = () => { 
  return (
    <div className={styles.header}>

      <a href="#default" className="logo">MyLogo</a>
    
      <div class="header-right">
        <Link to="/">Home</Link>
        <Link to="/about">About</Link>
        <Link to="/login">Login</Link>
        <Link to="/daftarmovie">Daftar Movie</Link>
      </div>

    </div> 
  )
} 

export default HeaderComponent;

/*const Nav = () => {
  return (
    <>
      <div className="Top">
          <Link to="/">Tugas_9</Link>
          <Link to="/tugas_10">Tugas_10</Link>
          <Link to="/tugas_11">Tugas_11</Link>
      </div>
    </>
  )
}

export default Nav*/