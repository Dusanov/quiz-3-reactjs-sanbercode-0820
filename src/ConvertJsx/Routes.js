import React from "react";
import { Switch, Route } from "react-router";

import Home from "./Home";
import About from "./About";
import Login from "./Login_v2";

import {TandaLoginProvider} from "./ContextTandaLogin"

import {MovieProvider} from "./Movie/DaftarMovie";
import MovieForm from "./Movie/MovieForm";
import MovieList from "./Movie/MovieList";

import styles from '../teststyle.module.css';

const Routes = () => {

  return (
    <Switch>
      <Route exact path="/">
        <div className={styles.containercontent}>
          <Home />
        </div>
      </Route>

      <Route path="/about">
        <div className={styles.containercontent}>
          <About />
        </div>
      </Route>

      <TandaLoginProvider>

      <Route exact path="/login">
        <div className={styles.containercontent}>
          <Login />
        </div>
      </Route>

      <Route exact path="/daftarmovie">
        <div className={styles.containercontent}>
          <MovieProvider>
              <MovieList /><br />
              <MovieForm />
          </MovieProvider >
        </div>
      </Route>

      </TandaLoginProvider>

    </Switch>

  );
};

export default Routes;