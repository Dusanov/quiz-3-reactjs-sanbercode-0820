import React, { useState, useContext } from 'react';
import {ContextTandaLogin} from "./ContextTandaLogin";

function Login() {

  const [isLogin, setIsLogin] = useContext(ContextTandaLogin)

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const handleChange_username = (event) => {
    var value = event.target.value;
    setUsername(value);
  }

  const handleChange_password = (event) => {
    var value = event.target.value;
    setPassword(value);
  }

  const handleSubmit_login = (event) => {

    event.preventDefault();

    if (username === "admin" && password === "admin"){
      setIsLogin(1);
      console.log(isLogin);
    }
  }


  return (
    <form onSubmit={handleSubmit_login} >
      
      Username <input type="text" onChange={handleChange_username}/><br/>
      Password <input type="password" onChange={handleChange_password}/><br/>
      <br/>
      <button>submit</button>
    
    </form>
  );
}

export default Login;


/*import React from 'react';
import ReactDOM from 'react-dom';

class Login extends React.Component {
    
    render() { 
        return (
          <div>
            <h3>Login</h3>
          </div>
         )
    }
}
 
export default Login ;*/