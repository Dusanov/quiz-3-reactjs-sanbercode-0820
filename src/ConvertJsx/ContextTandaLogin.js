import React, { useState, createContext } from "react";

export const ContextTandaLogin = createContext();

export const TandaLoginProvider = props => {

	const [isLogin, setIsLogin] = useState(0);

	return(
		<ContextTandaLogin.Provider value={[isLogin, setIsLogin]}>
			{props.children}
		</ContextTandaLogin.Provider>
	);
};