import React, {useContext} from "react"
import {DaftarMovie} from "./DaftarMovie";

import {ContextTandaLogin} from "../ContextTandaLogin"

const MovieForm = () => {

  const [movie, setMovie, inputForm, setInputForm] = useContext(DaftarMovie)
  const [isLogin, setIsLogin] = useContext(ContextTandaLogin)
  
  const handleSubmit = (event) => {
    
    event.preventDefault()

    var newId = movie.length +1

    if (inputForm.id === null) {
      setMovie([...movie, {title: inputForm.title, description:inputForm.description, years:inputForm.years, duration: inputForm.duration, genre: inputForm.genre, rating: inputForm.rating, id: newId}])
      }else{
        var singleMovie = movie.find(x=> x.id === inputForm.id)

        singleMovie.title = inputForm.title
        singleMovie.description = inputForm.description
        singleMovie.years = inputForm.years
        singleMovie.duration = inputForm.duration
        singleMovie.genre = inputForm.genre
        singleMovie.rating = inputForm.rating
        setMovie([...movie])
      }
    
    setInputForm({title: "", description: "", years:0, duration:"", genre: "", rating:"", id: null})  
  }
  
  const handleChangeTitle = (event) =>{
    setInputForm({...inputForm, title: event.target.value})
  
  }

  const handleChangeDescription = (event) =>{
    setInputForm({...inputForm, description: event.target.value})
    //setPrice(event.target.value)
  }

  const handleChangeYears = (event) =>{
    setInputForm({...inputForm, years: event.target.value})
    //setWeight(event.target.value)
  }

  const handleChangeDuration = (event) =>{
    setInputForm({...inputForm, duration: event.target.value})
    //setWeight(event.target.value)
  }

  const handleChangeGenre = (event) =>{
    setInputForm({...inputForm, genre: event.target.value})
    //setWeight(event.target.value)
  }

  const handleChangeRating = (event) =>{
    setInputForm({...inputForm, rating: event.target.value})
    //setWeight(event.target.value)
  }

  const fungsiRender = () => {
        var sudahLogin = isLogin;
    
        if(sudahLogin==0){

          return(null)
        
        } else {
          
          return(
            <>

              <h1>Movies Form</h1>

              <form onSubmit={handleSubmit}>
                
                <strong>Title</strong><input type="text" value={inputForm.title} onChange={handleChangeTitle} /><br/>
                <strong>Description</strong><input type="text" value={inputForm.description} onChange={handleChangeDescription} /><br/>
                <strong>Years</strong><input type="number" value={inputForm.years} onChange={handleChangeYears} /><br/>
                <strong>Duration</strong><input type="text" value={inputForm.duration} onChange={handleChangeDuration} /><br/>
                <strong>Genre</strong><input type="text" value={inputForm.genre} onChange={handleChangeGenre} /><br/>
                <strong>Rating</strong><input type="text" value={inputForm.rating} onChange={handleChangeRating} />
                <br/>
                <button>submit</button>
              
              </form>
            </>
          )
        }
  }

  return(
    fungsiRender()
  )

}

export default MovieForm;