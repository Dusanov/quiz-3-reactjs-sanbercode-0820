import React, { useState, createContext } from "react";

export const DaftarMovie = createContext();

export const MovieProvider = props => {

	 const [movie, setMovie] = useState([

	 	{ id: 1, title: "Avenger Invinity War", description: "Perang Saudara Antar Avenger", years: 2005, duration: "90", genre: "Action", rating: "8.0" } ,
		{ id: 2, title: "Long Shot", description: "Tentang seorang wartawan yang bertemu kembali dengan bekas pengasuhnya", years: 2000, duration: "90", genre: "Comedy", rating: "7.0" } ,
	 	{ id: 3, title: "Maze Runner", description: "Terkurung di labirin", years: 20015, duration: "90", genre: "Action", rating: "6.0" } ,
	 	{ id: 4, title: "Its Okay, To Not Be Okay", description: "Cerita Percintaan Korea", years: 2007, duration: "90", genre: "Drama", rating: "6.0" }
	]);

	const [inputForm, setInputForm] = useState({
			title: "",
			description: "",
			years: 0,
			duration:"",
			genre: "",
			rating: "",
			id: null
	})


	return (
	 	<DaftarMovie.Provider value = {[movie, setMovie, inputForm, setInputForm]}>
	 		{props.children}
	 	</DaftarMovie.Provider>
	);
};